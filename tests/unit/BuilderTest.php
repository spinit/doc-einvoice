<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Document\eInvoice\Test\Builder;

use Spinit\Document\eInvoice\Builder;
use Spinit\Document\eInvoice\Validator;
use PHPUnit\Framework\TestCase;

/**
 * Description of BuilderTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class BuilderTest extends TestCase
{
    /**
     *
     * @var Builder
     */
    private $object;
    
    protected function setUp()
    {
        $this->object = new Builder();
        parent::setUp();
    }
    
    public function testBuilder()
    {
        $this->assertNotNull($this->object);
        $this->object->setDatiTrasmissione("abcdefg","001");
        $this->assertEquals("IT", $this->object->get('FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdPaese'));
        $this->assertEquals("ABCDEFG", $this->object->get('FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdCodice'));
        $this->assertEquals("001", $this->object->get('FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio'));
    }
    
    public function testXml()
    {
        $this->testBuilder();
        $this->object->appendRiga('01', 'Riga di test &', '10', '5', '50', '22');
        $this->object->appendRiga('02', 'Riga > di test', '10', '5', '50', '22');
        $content = <<<EOCONT
<?xml version="1.0"?>
<p:FatturaElettronica xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:p="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" versione="FPR12" xsi:schemaLocation="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd">
  <FatturaElettronicaHeader>
    <DatiTrasmissione>
      <IdTrasmittente>
        <IdPaese>IT</IdPaese>
        <IdCodice>ABCDEFG</IdCodice>
      </IdTrasmittente>
      <ProgressivoInvio>00001</ProgressivoInvio>
      <FormatoTrasmissione>FPR12</FormatoTrasmissione>
      <CodiceDestinatario>0000000</CodiceDestinatario>
    </DatiTrasmissione>
  </FatturaElettronicaHeader>
  <FatturaElettronicaBody>
    <DatiBeniServizi>
      <DettaglioLinee>
        <NumeroLinea>01</NumeroLinea>
        <Descrizione>Riga di test &amp;</Descrizione>
        <Quantita>10.00</Quantita>
        <PrezzoUnitario>5.00000000</PrezzoUnitario>
        <PrezzoTotale>50.00</PrezzoTotale>
        <AliquotaIVA>22.00</AliquotaIVA>
      </DettaglioLinee>
      <DettaglioLinee>
        <NumeroLinea>02</NumeroLinea>
        <Descrizione>Riga &gt; di test</Descrizione>
        <Quantita>10.00</Quantita>
        <PrezzoUnitario>5.00000000</PrezzoUnitario>
        <PrezzoTotale>50.00</PrezzoTotale>
        <AliquotaIVA>22.00</AliquotaIVA>
      </DettaglioLinee>
    </DatiBeniServizi>
  </FatturaElettronicaBody>
</p:FatturaElettronica>
EOCONT;
        $this->assertEquals(trim($content), trim($this->object->getXml()));
    }
    
    public function testXmlValidator()
    {
        return;
        $builder = $this->object;
        $builder->setDatiGeneraliDocumento('TD01', 'EUR', '2019-01-01', 'A/1', '100', '0', '');
        $builder->setDatiTrasmissione('01010101010', '00000', '', '0000000', '', 'IT');
        $builder->setCedentePrestatore('RF18', '1010101010101', 'adfsafkdjasldfkj', 'Prestatore');
        $builder->setCedentePrestatoreSede('Via test', '', '55555', 'Cittadina','prv','IT');
        $builder->setCessionarioCommittente(['IT', '212313211'], 'fasdfasfsaf', 'Committente');
        $builder->setCessionarioCommittenteSede('Via prova', '', '44444', 'POTENZA', 'PZ', 'IT');
        $builder->appendRiga('01', 'Riga di test &', '10', '5', '50', '22');
        $builder->appendRiga('02', 'Riga > di test', '10', '5', '50', '22');
        $builder->setNoteFattura('test note');
        
        $validator = new Validator();
        $this->assertTrue($validator->validateFeed($builder->getXml()), print_r($validator->displayErrors(), 1));
    }
}
