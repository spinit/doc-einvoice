<?php
namespace Spinit\Document\eInvoice;

use Spinit\Document\eInvoice\Builder;

/**
 * Description of ElectronicInvoiceFactory
 *
 * @author Pietro Celeste <p.celeste@spinit.it>
 */
class ErpFactory
{
    private $db;    
    private $xmls = [];
    private $cedenteTaxCode;
    private $cedenteCountry = 'IT';
    
    public function __construct($db)
    {
        $this->db = $db;        
    }
    
    private function getDb()
    {
        return $this->db;
    }
    
    public function buildInvoices($ids)
    {
        $invoices = [];
        foreach($ids as $invoiceId){
            $invoices[$invoiceId] = [];
            $invoices[$invoiceId]['head'] = $this->getHead($invoiceId);
            $invoices[$invoiceId]['cedente'] = $this->getAna($invoices[$invoiceId]['head']['DittaID']);
            $invoices[$invoiceId]['cedenteSede'] = $this->getSede($invoices[$invoiceId]['head']['DittaID']);
            $invoices[$invoiceId]['cessionario'] = $this->getAna($invoices[$invoiceId]['head']['ClienteID']);
            $invoices[$invoiceId]['cessionarioSede'] = $this->getSede($invoices[$invoiceId]['head']['ClienteID']);
            $invoices[$invoiceId]['rows'] = $this->getRows($invoiceId);
            $invoices[$invoiceId]['summaries'] = $this->getSummary($invoiceId);
        }
        foreach($invoices as $id => $invoice) {
            $xml = str_replace(
                [
                    '<FatturaElettronica>',
                    '</FatturaElettronica>'
                ],
                [
                    '<p:FatturaElettronica xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:p="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" versione="FPA12" xsi:schemaLocation="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 fatturaordinaria_v1.2.xsd">',
                    '</p:FatturaElettronica>'
                ],
                $this->buildElectronicInvoice($invoice)
            );
            $validator = new Validator();
            if ($validator->validateFeed($xml)) {
                $this->xmls[$id] =  $xml;
                continue;
            }
            die(print_r($validator->displayErrors(),true));
        }        
    }
        
    private function buildElectronicInvoice($invoice)
    {
        if (empty($this->cedenteTaxCode)) {
            $this->cedenteTaxCode = $invoice['cedente']['CodiceFiscale'];
        }
        $Helper = new Builder();
        $Helper->setDatiTrasmissione(
            $invoice['cedente']['CodiceFiscale'], 
            $invoice['head']['FatturaID'],
            $invoice['cessionario']['Pec'],
            $invoice['cessionario']['CodiceCanale']
            //$invoice['cedente']['Telefono'], 
            //$invoice['cedente']['Email']
        );
        $Helper->setDatiGeneraliDocumento(
            $invoice['head']['TipoDocumento'],
            $invoice['head']['Divisa'],
            $invoice['head']['Data'],
            $invoice['head']['Numero'],
            $invoice['head']['ImportoTotaleDocumento'],
            $invoice['head']['Arrotondamento'],
            $invoice['head']['Causale']
        );
        //Accodo i dati relativi al Cedente
        $Helper->setCedentePrestatore(
            $invoice['cedente']['RegimeFiscale'], 
            $invoice['cedente']['PartitaIva'], 
            $invoice['cedente']['CodiceFiscale'], 
            $invoice['cedente']['RagioneSociale'], 
            $invoice['cedente']['Nome'], 
            $invoice['cedente']['Cognome'], 
            $invoice['cedente']['Titolo'], 
            $invoice['cedente']['CodiceEORI'], 
            $invoice['cedente']['AlboProfessionale'], 
            $invoice['cedente']['ProvinciaAlbo'],
            $invoice['cedente']['NumeroIscrizioneAlbo'],
            $invoice['cedente']['DataIscrizioneAlbo'],
            $invoice['cedente']['IdPaese']
        );
        //Accodo i dati relativi alla sede del Cedente
        $Helper->setCedentePrestatoreSede(
            $invoice['cedenteSede']['Indirizzo'], 
            $invoice['cedenteSede']['NumeroCivico'], 
            $invoice['cedenteSede']['CAP'], 
            $invoice['cedenteSede']['Comune'], 
            $invoice['cedenteSede']['Provincia'], 
            $invoice['cedenteSede']['Nazione']
        );
        //Accodo i dati relativi al Cessionario
        $Helper->setCessionarioCommittente(            
            $invoice['cessionario']['PartitaIva'], 
            $invoice['cessionario']['CodiceFiscale'], 
            $invoice['cessionario']['RagioneSociale'], 
            $invoice['cessionario']['Nome'], 
            $invoice['cessionario']['Cognome'], 
            $invoice['cessionario']['Titolo'], 
            $invoice['cessionario']['CodiceEORI'], 
            $invoice['cessionario']['AlboProfessionale'], 
            $invoice['cessionario']['ProvinciaAlbo'],
            $invoice['cessionario']['NumeroIscrizioneAlbo'],
            $invoice['cessionario']['DataIscrizioneAlbo'],
            $invoice['cessionario']['IdPaese']
        );
        //Accodo i dati relativi alla sede del Cessionario
        $Helper->setCessionarioCommittenteSede(
            $invoice['cessionarioSede']['Indirizzo'], 
            $invoice['cessionarioSede']['NumeroCivico'], 
            $invoice['cessionarioSede']['CAP'], 
            $invoice['cessionarioSede']['Comune'], 
            $invoice['cessionarioSede']['Provincia'], 
            $invoice['cessionarioSede']['Nazione']
        );
        foreach($invoice['rows'] as $row) {
            $Helper->appendRiga(
                $row['NumeroLinea'], 
                $row['Descrizione'], 
                $row['Quantita'], 
                $row['PrezzoUnitario'], 
                $row['PrezzoTotale'], 
                $row['AliquotaIva'],
                $row['UnitaMisura'],
                $row['TipoCessionePrestazione'], 
                $row['CodiceTipo'],
                $row['CodiceValore'], 
                $row['ScontoMaggiorazioneTipo'],
                $row['ScontoMaggiorazionePercentuale'],
                $row['ScontoMaggiorazioneImporto'], 
                $row['DataInizioPeriodo'],
                $row['DataFinePeriodo'], 
                $row['Ritenuta'],
                $row['Natura'],
                $row['RiferimentoAmministrazione']
            );
        }
        foreach($invoice['summaries'] as $summary) {
            $Helper->setDatiRiepilogo(
                $summary['AliquotaIva'], 
                $summary['Natura'], 
                $summary['SpeseAccessorie'], 
                $summary['Arrotondamento'], 
                $summary['ImponibileImporto'], 
                $summary['Imposta'],
                $summary['EsigibilitaImposta'],
                $summary['RiferimentoNormativo']
            );
        }
        return $Helper->getXml();
    }
    
    private function getHead($id)
    {                
        return $this->db->execUnique(
            "SELECT i.id          as FatturaID,
                    i.ana_cmp     as DittaID,
                    i.ana_slv     as ClienteID,
                    t.fat_ele_cod as TipoDocumento,
                    'EUR'         as Divisa,
                    DATE_FORMAT(i.dat,'%Y-%m-%d') as Data,
                    i.num_def     as Numero,
                    i.tot_grd     as ImportoTotaleDocumento,
                    0.00          as Arrotondamento,
                    null          as Causale
             FROM tbl_doc i
             LEFT JOIN tbl_doc_typ t ON (i.id_typ = t.id)
             WHERE i.id = ?",
            [$id],
            'ASSOC'
        );
    }
    
    private function getAna($anaId)
    {
        $cedente = $this->getDb()->execUnique(
            "SELECT ifnull(rf.val,'RF01')  as RegimeFiscale,
                    a.vat_num              as PartitaIva,
                    a.tax_cod              as CodiceFiscale,
                    a.soc_nam              as RagioneSociale,
                    a.prs_frt_nam          as Nome,
                    a.prs_lst_nam          as Cognome,
                    null                   as Titolo,
                    null                   as CodiceEORI,
                    null                   as AlboProfessionale,
                    null                   as ProvinciaAlbo, 
                    null                   as NumeroIscrizioneAlbo,
                    null                   as DataIscrizioneAlbo, 
                    'IT'                   as IdPaese,
                    eml_2                  as Pec,
                    ifnull(null,'0000000') as CodiceCanale,
                    ifnull(tel_1,tel_2)    as Telefono,
                    ifnull(eml_1,eml_2)    as Email
             FROM tbl_ana a
             LEFT JOIN tbl_ana_prp rf ON (a.id = rf.id_ana AND rf.id_prp = 'companyTaxRegime')
             WHERE a.id = ?",
            [$anaId],
            'ASSOC'
        );
        return $cedente;
    }        
    
    private function getSede($anaId)
    {
        return $this->getDb()->execUnique(
            "SELECT a.add_add as Indirizzo,
                    null      as NumeroCivico,
                    a.add_zip as CAP,
                    a.add_cit as Comune,
                    a.add_prv as Provincia,
                    ifnull(a.add_cau,'IT') as Nazione
             FROM tbl_ana_add a
             WHERE a.id_typ = 1 AND a.id_ana = ?",
            [$anaId],
            'ASSOC'
        );        
    }
    
    public function getRows($invoiceId)
    {        
        $this->getDb()->execCommand(
            "SET @irow = 0;"
        );
        return $this->getDb()->execQuery(
            "SELECT @irow := @irow + 1 as NumeroLinea,
                    r.itm             as Descrizione,
                    r.qty             as Quantita,
                    r.prc             as PrezzoUnitario,
                    r.tot_tax                     as PrezzoTotale,
                    v.prc                         as AliquotaIva,
                    r.um                          as UnitaMisura,
                    null                          as TipoCessionePrestazione,
                    null                          as CodiceTipo,
                    null                          as CodiceValore,
                    if(dis_prc is null,null,'SC') as ScontoMaggiorazioneTipo,
                    dis_prc                       as ScontoMaggiorazionePercentuale,
                    dis_tot                       as ScontoMaggiorazioneImporto,
                    null                          as DataInizioPeriodo,
                    null                          as DataFinePeriodo,
                    null                          as Ritenuta,
                    v.nat                         as Natura,
                    null                          as RiferimentoAmministrazione
             FROM tbl_doc_row r
             LEFT JOIN tbl_vat v ON (r.id_vat = v.id)
             WHERE r.id_doc = :documentId",
             ['documentId' => $invoiceId],
             'ASSOC'
        );
    }
    
    public function getSummary($invoiceId)
    {
        return $this->getDb()->execQuery(
            "SELECT v.prc               as AliquotaIva,
                    v.nat               as Natura,
                    null                as SpeseAccessorie,
                    null                as Arrotondamento,
                    sum(r.tot_tax)      as ImponibileImporto,  
                    sum(r.tot_vat)      as Imposta,
                    'I'                 as EsigibilitaImposta,
                    v.nat_dsc           as RiferimentoNormativo
             FROM tbl_doc_row r
             LEFT JOIN tbl_vat v ON (r.id_vat = v.id)
             WHERE r.id_doc = :documentId
             GROUP BY v.prc, v.nat",
             ['documentId' => $invoiceId],
             'ASSOC'
        );
    }
    
    public function getInvoices($id = null)
    {
        return empty($id) ? $this->xmls : $this->xmls[$id];
    }
    
    public function getZipFile()
    {    
        if (empty($this->xmls)) {
            return 'Nessuna fattura da esportare. Il file di export non sarà generato.';
        }
        $basePath = '/upload/export/';
        $baseName = $this->cedenteCountry.$this->cedenteTaxCode;
        $zipName = $basePath.$baseName.'_00001.zip';
        $zip = new \ZipArchive();
        $zip->open($_SERVER['DOCUMENT_ROOT'].$zipName, \ZipArchive::CREATE|\ZipArchive::OVERWRITE);        
        foreach($this->getInvoices() as $invoiceId => $invoiceXml) {
            $fileId = base_convert($invoiceId, 10, 32);
            $zip->addFromString($baseName.'_'.str_pad($fileId,5,'0',STR_PAD_LEFT).'.xml', $invoiceXml);            
        }
        $zip->close();
        return $zipName;
    }
}
