<?php
namespace Spinit\Document\eInvoice;

use Spinit\Util\Error\NotFoundException;

class Validator
{
    /**
     * @var string
     */
    protected $feedSchema;
    /**
     * @var int
     */
    public $feedErrors = 0;
    /**
     * Formatted libxml Error details
     *
     * @var array
     */
    public $errorDetails;
    
    private $handler;
    
    /**
     * Validation Class constructor Instantiating DOMDocument
     *
     * @param \DOMDocument $handler [description]
     */
    public function __construct()
    {
        $this->feedSchema = dirname(__DIR__)  .  '/res/FatturaElettronica-1.2.1.xsd';
        $this->handler = new \DOMDocument('1.0', 'utf-8');                
    }
    /**
     * @param \libXMLError object $error
     *
     * @return string
     */
    private function libxmlDisplayError($error)
    {
        $errorString = "Error $error->code in $error->file (Line:{$error->line}):";
        $errorString .= trim($error->message);
        return $errorString;
    }
    /**
     * @return array
     */
    private function libxmlDisplayErrors()
    {
        $errors = libxml_get_errors();
        $result    = [];
        foreach ($errors as $error) {
            $result[] = $this->libxmlDisplayError($error);
        }
        libxml_clear_errors();
        return $result;
    }
    /**
     * Validate Incoming Feeds against Listing Schema
     *
     * @param resource $feeds
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function validateFeeds($feeds)
    {
        if (!class_exists('DOMDocument')) {
            throw new \DOMException("'DOMDocument' class not found!");            
        }
        if (!file_exists($this->feedSchema)) {
            throw new \Exception('Schema is Missing, Please add schema to feedSchema property');            
        }
        if (!($fp = fopen($feeds, "r"))) {
           throw new NotFoundException("could not open XML input : {$feeds}");
        }
        
        $contents = fread($fp, filesize($feeds));
        fclose($fp);
       
        $this->validateFeed($contents);
    }
    
    public function validateFeed($xmlContent)
    {
        libxml_use_internal_errors(true);
        $this->handler->loadXML($xmlContent, LIBXML_NOBLANKS);
        //The file is valid
        if ($this->handler->schemaValidate($this->feedSchema)) {
            return true;
        } 
        $this->errorDetails = $this->libxmlDisplayErrors();
        $this->feedErrors   = 1;
        return false;                    
    }
    /**
     * Display Error if Resource is not validated
     *
     * @return array
     */
    public function displayErrors()
    {
        return $this->errorDetails;
    }
}

