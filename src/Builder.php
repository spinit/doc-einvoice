<?php
namespace Spinit\Document\eInvoice;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Util\DictionaryBase as Dictionary;

/**
 * Description of Builder
 *
 * @author Peter
 */
class Builder
{
    private $invoiceDataCheck = [
        'trasmission' => false,
        'cedente' => false,
        'cessionario' => false,
        'rows' => false,
        'summary' => false
    ];
    private $modalitaPagamento = [];
    private $detailInvoice = [];
    private $summaryInvoice = [];
    private $dict;
    private $docType = 'FPR12';
    
    public function __construct()
    {
        //$this->dict = new \Osynapsy\Data\Dictionary($this->dataStructure);
        $this->dict = new Dictionary(['FatturaElettronicaHeader'=>[]]);                      
    }
    
    public function setTypePA()
    {
        $this->docType = 'FPA12';
    }
    public function getXml()
    {
        $attr = ['xmlns:ds'=>"http://www.w3.org/2000/09/xmldsig#", 
            'xmlns:p'=>"http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2",
            'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
            'versione'=>$this->docType,
            'xsi:schemaLocation'=>"http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd"
        ];
        $data = [$this->detailInvoice];
        foreach($this->summaryInvoice as $rec) {
            $data[] = $rec;
        }
        $this->dict->set('FatturaElettronicaBody.DatiBeniServizi', $data);        
        
        $datiPagamento = [];
        if (count($this->modalitaPagamento)) {
            foreach($this->modalitaPagamento as $k => $pag) {
                $datiPagamento []= ['CondizioniPagamento'=>$pag['type']];
                foreach($pag['data'] as $p) {
                    $datiPagamento []=['DettaglioPagamento'=>$p];
                }
            }
            $this->dict->set('FatturaElettronicaBody.DatiPagamento', $datiPagamento); 
        }
        return str_replace( ['<FatturaElettronica ', '</FatturaElettronica>'], 
                            ['<p:FatturaElettronica ', '</p:FatturaElettronica>'], 
                            $this->xmlSerialize('FatturaElettronica', $attr));
    }
    
    private function xmlSerialize($rootElement = 'root', $attr = [])
    {
        $attrStr = '';
        foreach($attr as $k=>$v) {
            $attrStr .= ' '.$k.'="'.htmlentities($v).'"';
        }
        $xml = new \SimpleXMLElement('<'.$rootElement.$attrStr.'/>');
        $arrayToXml = function($data, $xml) use (&$arrayToXml) {
            foreach($data as $key => $value) {
                if(is_numeric($key)){
                    $arrayToXml($value, $xml);
                } else if (is_array($value)) {
                    if (!$key) {
                        var_dump($data);exit;
                    }
                    $subnode = $xml->addChild($key);
                    $arrayToXml($value, $subnode);                             
                } else if ($value) {
                    $xml->addChild($key, htmlentities($value));
                }
            }
        };
        call_user_func($arrayToXml, $this->dict, $xml);
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;
        return $dom->saveXML();
        //return str_replace('><','>'.PHP_EOL.'<',$xml->asXML());
    }
    
    public function arrayToXml($data, &$xml)
    {
    }
    /**
     * 
     * @param string $CodiceFiscale Codice fiscale del soggetto che sta effettuando la trasmissione della fattura
     * @param string $ProgressivoInvio Id progressivo dell'invio
     * @param string $CodiceDestinatario Codice del destinatario
     * @param string $TelefonoTrasmittente Telefono del soggetto che sta effettuando la trasmissione
     * @param string $EmailTrasmittente Email del soggetto che sta effettando la trasmissione
     * @param string $idPaese Id del paese del destinatario
     */    
    public function setDatiTrasmissione($CodiceFiscale, $ProgressivoInvio, $PECDestinatario = null, $CodiceDestinatario = '0000000',  $TelefonoTrasmittente = '', $EmailTrasmittente = '', $IdPaese = 'IT')
    {
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente', 'IdPaese', $IdPaese);
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente', 'IdCodice', strtoupper($CodiceFiscale));
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione', 'ProgressivoInvio', str_pad($ProgressivoInvio, 5 ,'0', STR_PAD_LEFT));
        //Vedi istruzioni pag.29 - Allegato A - Specifiche tecniche v.1.1
        //FormatoTrasmissione: codice identificativo del tipo di trasmissione
        //che si sta effettuando e del relativo formato. Va sempre valorizzato 
        //con "FPR12".        
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione', 'FormatoTrasmissione', $this->docType);        
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione', 'CodiceDestinatario', strtoupper($CodiceDestinatario));
        $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione', 'PECDestinatario', $PECDestinatario);
        //$this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione.ContattiTrasmittente', 'Telefono', $TelefonoTrasmittente);
        
        // Se viene indicata la pec del destinatario non occorre indicare l'email del trasmittente
        if (strlen(trim($PECDestinatario)) == 0) {
            $this->setFieldValue('FatturaElettronicaHeader.DatiTrasmissione.ContattiTrasmittente', 'Email', $EmailTrasmittente);
        }
        $this->invoiceDataCheck['trasmission'] = true;
    }
    
    public function setCedentePrestatore($RegimeFiscale, $PartitaIva, $CodiceFiscale, $RagioneSociale = null, $Nome = null, $Cognome = null, $Titolo = null, $CodiceEORI = null, $AlboProfessionale = null, $ProvinciaAlbo = '', $NumeroIscrizioneAlbo = '', $DataIscrizioneAlbo = '', $IdPaese = 'IT')
    {                
        $basePath = 'FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici';
        $this->setModelAnagrafica($basePath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);        
        $this->setFieldValue($basePath, 'AlboProfessionale', $AlboProfessionale);
        $this->setFieldValue($basePath, 'ProvinciaAlbo', $ProvinciaAlbo);
        $this->setFieldValue($basePath, 'NumeroIscrizioneAlbo', $NumeroIscrizioneAlbo);
        $this->setFieldValue($basePath, 'DataIscrizioneAlbo', $DataIscrizioneAlbo);
        /*
        *   RegimeFiscale: formato alfanumerico; lunghezza di 4 caratteri; i valori ammessi sono i seguenti:
        *   RF01  Ordinario;  
        *   RF02  Contribuenti minimi (art. 1, c.96-117, L.244/2007);  
        *   RF04  Agricoltura e attività connesse e pesca (artt. 34 e34-bis, D.P.R. 633/1972);  
        *   RF05  Vendita sali e tabacchi (art. 74, c.1, D.P.R.633/1972);RF06  Commercio dei fiammiferi (art. 74, c.1, D633/1972);  
        *   RF07  Editoria (art. 74, c.1, D.P.R. 633/1972); 
        *   RF08  Gestione di servizi di telefonia pubblica (ac.1, D.P.R.  633/1972); 
        *   RF09  Rivendita di documenti di trasporto pubblicsosta (art. 74, c.1, D.P.R. 633/1972); 
        *   RF10  Intrattenimenti, giochi e altre attività di cu tariffa allegata al D.P.R. n. 640/72 (art. 74D.P.R. 633/1972);  
        *   RF11  Agenzie di viaggi e turismo (art. 74-ter, D633/1972);  
        *   RF12  Agriturismo (art. 5, c.2, L. 413/1991); 
        *   RF13  Vendite a domicilio (art. 25-bis, c.6, D600/1973); 
        *   RF14  Rivendita di beni usati, di oggetti d’antiquariato o da collezione (art. 36,41/1995);  
        *   RF15  Agenzie di vendite all’asta di oggetti antiquariato o da collezione (art. 40-bis41/1995);  
        *   RF16  IVA per cassa P.A. (art. 6, c.5, D.P.R. 633/1RF17 IVA per cassa (art. 32-bis, D.L. 83/2012);
        *   RF18  Altro;  
        *   RF19  Forfettario (art.1, c. 54-89, L. 190/2014); 
        */
        $this->setFieldValue($basePath, 'RegimeFiscale', $RegimeFiscale);
        $this->invoiceCheck['cedente'] = true;
    }
    
    public function setCedentePrestatoreSede($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione)
    {
        $path = 'FatturaElettronicaHeader.CedentePrestatore.Sede';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);        
    }
    
    public function setCedentePrestatoreStabileOrganizzazione($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione = 'it-IT')
    {
        $path = 'FatturaElettronicaHeader.CedentePrestatore.StabileOrganizzazione';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);        
    }
    
    public function setCedentePrestatoreIscrizioneRea($Ufficio, $NumeroRea, $CapitaleSociale, $SocioUnico = 'SM', $StatoLiquidazione = 'LN')
    {        
        /*
         *  Ufficio: formato alfanumerico; lunghezza di 2 caratteri.
         */
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.IscrizioneRea.Ufficio', $Ufficio);
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.IscrizioneRea.NumeroRea', $NumeroRea);
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.IscrizioneRea.CapitaleSociale', $CapitaleSociale);
        /*
         *  SocioUnico: formato alfanumerico; lunghezza di 2 caratteri; i valoammessi sono i seguenti: 
         *  SU   la società è a socio unico.
         *  SM   la società NON è a socio unico. 
         */
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.IscrizioneRea.SocioUnico', $SocioUnico);
        /*
         *  StatoLiquidazione: formato alfanumerico; lunghezza di 2 caratteri; i valori ammessi sono i seguenti: 
         *  LS   la società è in stato di liquidazione.
         *  LN   la società NON è in stato di liquidazione. 
         */
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.IscrizioneRea.StatoLiquidazione', $StatoLiquidazione);        
    }
    
    public function setCedentePrestatoreContatti($RiferimentoAmministrativo = '', $Telefono = '', $Fax = '', $Email = '')
    {        
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.RiferimentoAmministrazione', $RiferimentoAmministrativo);
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.Contatti.Telefono', $Telefono);
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.Contatti.Fax', $Fax);
        $this->dict->set('FatturaElettronicaHeader.CedentePrestatore.Contatti.Email', $Email);
    }
    
    public function setCedentePrestatoreRappresentanteFiscale($PartitaIva, $CodiceFiscale, $RagioneSociale = '', $Nome = '', $Cognome = '', $Titolo = '', $CodiceEORI = '', $IdPaese = 'IT')
    {                
        $basepath = 'FatturaElettronicaHeader.CedentePrestatore.RappresentanteFiscale.DatiAnagrafici';
        $this->setModelAnagrafica($basepath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);        
    }
    
    public function setCedentePrestatoreRappresentateFiscaleSede($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione = 'it-IT')
    {        
        $path = 'FatturaElettronicaHeader.CedentePrestatore.RappresentanteFiscale.Sede';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);
    }
            
    public function setCessionarioCommittente($PartitaIva, $CodiceFiscale, $RagioneSociale = '', $Nome = '', $Cognome = '', $Titolo = '', $CodiceEORI = '', $AlboProfessionale = '', $ProvinciaAlbo = '', $NumeroIscrizioneAlbo = '', $DataIscrizioneAlbo = '', $IdPaese = 'IT')
    {   
        if (is_array($PartitaIva)) {
            $IdPaese = array_shift($PartitaIva);
            $PartitaIva = array_shift($PartitaIva);
        }
        $basePath = 'FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici';
        $this->setModelAnagrafica($basePath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);
        $this->setFieldValue($basePath, 'AlboProfessionale', $AlboProfessionale);
        $this->setFieldValue($basePath, 'ProvinciaAlbo', $ProvinciaAlbo);
        $this->setFieldValue($basePath, 'NumeroIscrizioneAlbo', $NumeroIscrizioneAlbo);
        $this->setFieldValue($basePath, 'DataIscrizioneAlbo', $DataIscrizioneAlbo);            
    }
    
    public function setCessionarioCommittenteSede($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione = 'it-IT')
    {
        $path = 'FatturaElettronicaHeader.CessionarioCommittente.Sede';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);       
    }
    
    public function setCessionarioCommittenteStabileOrganizzazione($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione = 'it-IT')
    {
        $path = 'FatturaElettronicaHeader.CessionarioCommittente.StabileOrganizzazione';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);        
    }
    
    public function setCessionarioCommittenteRappresentanteFiscale($PartitaIva, $CodiceFiscale, $RagioneSociale = '', $Nome = '', $Cognome = '', $Titolo = '', $CodiceEORI = '', $IdPaese = 'IT')
    {        
        $basePath = 'FatturaElettronicaHeader.CessionarioCommittente.RappresentanteFiscale.DatiAnagrafici';
        $this->setModelAnagrafica($basePath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);
    }
    
    public function setCessionarioCommittenteRappresentateFiscaleSede($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione = 'it-IT')
    {
        $path = 'FatturaElettronicaHeader.CessionarioCommittente.RappresentanteFiscale.Sede';
        $this->setModelIndirizzo($path, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);        
    }
    
    public function setTerzoIntermediarioOSoggettoEmittente($PartitaIva, $CodiceFiscale, $RagioneSociale = '', $Nome = '', $Cognome = '', $Titolo = '', $CodiceEORI = '', $IdPaese = 'IT')
    {        
        $basePath = 'FatturaElettronicaHeader.TerzoIntermediarioOSoggettoEmittente.DatiAnagrafici';
        $this->setModelAnagrafica($basePath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);
    }
    
    /**
     * 
     * @param string $TipoDocumento          formato alfanumerico; lunghezza di 4 caratteri; ivalori ammessi sono i seguenti: 
     *                                       TD01  Fattura
     *                                       TD02  Acconto/Anticipo su fattura
     *                                       TD03  Acconto/Anticipo su parcella
     *                                       TD04  Nota di Credito
     *                                       TD05  Nota di Debito
     *                                       TD06  Parcella
     *                                       TD20  Autofattura 
     * @param string $Divisa                 questo campo deve essere espresso secondo lo standard ISO 4217 alpha-3:2001 (es.: EUR, USD, GBP, CZK………). 
     * @param string $Data                   la data deve essere rappresentata secondo il formato 8601:2004, con la seguente precisione: YYYY-MM-DD.
     * @param string $Numero                 formato alfanumerico; lunghezza massima di 20 caratteri.
     * @param float  $ImportoTotaleDocumento formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 15 caratteri.
     * @param float  $Arrotondamento         formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 15 caratteri.
     * @param string $Causale                formato alfanumerico; lunghezza massima di 200 caratteri.
     * @param string $Art73                  formato alfanumerico; lunghezza di 2 caratteri; il valore ammesso è: SI documento emesso secondo modalità e termini stabiliti con DM ai sensi del’’art. 73 del DPR633/72.
     */
    public function setDatiGeneraliDocumento($TipoDocumento, $Divisa, $Data, $Numero, $ImportoTotaleDocumento, $Arrotondamento, $Causale, $Art73 = null)
    {
        $basePath = 'FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento';        
        $this->setFieldValue($basePath, 'TipoDocumento', $TipoDocumento);        
        $this->setFieldValue($basePath, 'Divisa', $Divisa);
        $this->setFieldValue($basePath, 'Data', $Data);                        
        $this->setFieldValue($basePath, 'Numero', $Numero);       
        $this->setFieldValue($basePath, 'ScontoMaggiorazione', '', true);
        $this->setFieldValue($basePath, 'ImportoTotaleDocumento', $ImportoTotaleDocumento);
        $this->setFieldValue($basePath, 'Arrotondamento', $Arrotondamento);
        $this->setFieldValue($basePath, 'Causale', $Causale);
        $this->setFieldValue($basePath, 'Art73', $Art73);       
    }
    
    public function setNoteFattura($descr)
    {
        $basePath = 'FatturaElettronicaBody.Allegati12';        
        $this->setFieldValue($basePath, 'NomeAttachment', 'Note');
        $this->setFieldValue($basePath, 'FormatoAttachment', 'text');
        $this->setFieldValue($basePath, 'DescrizioneAttachment', 'Testo note fattura base64');
        $this->setFieldValue($basePath, 'Attachment', base64_encode($descr));
    }
    public function setDatiGeneraliDocumentoDatiRitenuta($TipoRitenuta, $ImportoRitenuta, $AliquotaRitenuta, $CausalePagamento)
    {
        /*
         * TipoRitenuta: formato alfanumerico; lunghezza di 4 caratteri; i valori ammessi sono i seguenti:
         * RT01   Ritenuta persone fisiche 
         * RT02   Ritenuta persone giuridiche
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiRitenuta.TipoRitenuta', $TipoRitenuta);
        /*
         * ImportoRitenuta: formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza va da 4 a 15 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiRitenuta.ImportoRitenuta', $ImportoRitenuta);
        /*
         * AliquotaRitenuta: formato numerico nel quale i decimali vannoseparati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza  da 4 a 6 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiRitenuta.AliquotaRitenuta', $AliquotaRitenuta);
        /*
         * CausalePagamento: formato alfanumerico; lunghezza di massimo 2 caratteri; 
         * i valori ammessi sono quelli del 770S consultabili alla pagina delle istruzioni di compilazione del modello. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiRitenuta.CausalePagamento', $CausalePagamento);
    }
    
    public function setDatiGeneraliDocumentoDatiBollo($ImportoBollo, $BolloVirtuale = 'SI')
    {        
        /*
         *  BolloVirtuale: formato alfanumerico, lunghezza di 2 caratteri; 
         *  il valore ammesso è SI bollo assolto ai sensi del decreto MEF 14 giugno 2014
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiBollo.BolloVirtuale', $BolloVirtuale);
        /*
         *  ImportoBollo: formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza va da 4 a 15 caratteri. 
         */
         $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiBollo.ImportoBollo', $ImportoBollo);
    }
    
    public function setDatiGeneraliDocumentoCassaPrevidenziale($TipoCassa, $AlCassa, $ImportoContributoCassa, $ImponibileCassa, $AliquotaIVA, $Ritenuta, $Natura, $RiferimentoAmministrazione)
    {              
        /*
         * TipoCassa: formato alfanumerico; lunghezza di 4 caratteri; i valori ammessi sono i seguenti:
         * TC01  Cassa Nazionale Previdenza e Assistenza Avvocati e Procuratori Legali
         * TC02   Cassa Previdenza Dottori Commercialisti
         * TC03   Cassa Previdenza e Assistenza Geometri
         * TC04  Cassa Nazionale Previdenza e Assistenza Ingegneri e Architetti Liberi Professionisti
         * TC05  Cassa Nazionale del Notariato
         * TC06   Cassa Nazionale Previdenza e Assistenza Ragionieri e Periti Commerciali 
         * TC07   Ente Nazionale Assistenza Agenti eRappresentanti di Commercio (ENASARCO) 
         * TC08   Ente Nazionale Previdenza e Assistenza Consulenti del Lavoro (ENPACL) 
         * TC09   Ente Nazionale Previdenza e Assistenza Medici (ENPAM)
         * TC10   Ente Nazionale Previdenza e Assistenza Farmacisti (ENPAF) 
         * TC11   Ente Nazionale Previdenza e Assistenza Veterinari (ENPAV)
         * TC12   Ente Nazionale Previdenza e Assistenza Impiegati dell'Agricoltura (ENPAIA)
         * TC13   Fondo Previdenza Impiegati Imprese di Spedizione e Agenzie Marittime
         * TC14  Istituto Nazionale Previdenza Giornalisti Italiani (INPGI)
         * TC15   Opera Nazionale Assistenza Orfani Sanitari Italiani (ONAOSI)
         * TC16   Cassa Autonoma Assistenza Integrativa Giornalisti Italiani (CASAGIT)
         * TC17   Ente Previdenza Periti Industriali e Periti Industriali Laureati (EPPI)
         * TC18   Ente Previdenza e Assistenza Pluricategoriale (EPAP)
         * TC19   Ente Nazionale Previdenza e Assistenza Biologi (ENPAB)
         * TC20   Ente Nazionale Previdenza e Assistenza Professione Infermieristica (ENPAPI)
         * TC21   Ente Nazionale Previdenza e Assistenza Psicologi (ENPAP)
         * TC22  INPS 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.TipoCassa', $TipoCassa);
        /*
         * AlCassa: formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza va da 4 a 6 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.AlCassa', $AlCassa);
        /*
         * ImportoContributoCassa:  formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza va da 4 a 15 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.ImportoContributoCassa', $ImportoContributoCassa);
        /*
         * ImponibileCassa: formato numerico nel quale i decinali vanno separati dall’intero con il carattere ‘.’ (punto). 
         * La sua lunghezza va da 4 a 15 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.ImponibileCassa', $ImponibileCassa);
        /*
         * AliquotaIVA: formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). 
         * La sua lunghezza va da 4 a 6 caratteri.
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.AliquotaIVA', $AliquotaIVA);
        /*
         * Ritenuta: formato alfanumerico; lunghezza di 2 caratteri; il valore ammesso è: 
         * SI  contributo cassa soggetto a ritenuta
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.TipoCassa', $Ritenuta);
        /*
         * Natura: formato alfanumerico; lunghezza di 2 caratteri; i valori  ammessi sono i seguenti:
         * N1  escluse ex art.15
         * N2  non soggette
         * N3  non imponibili
         * N4  esenti
         * N5  regime del margine / IVA non esposta in fattura 
         * N6  inversione contabile (per le operazioni in reverse charge ovvero nei casi di autofatturazione per 
         *     acquisti extra UE di servizi ovvero per importazioni di beni nei soli casi previsti) 
         * N7  IVA assolta in altro stato UE (vendite a distanza ex art. 40 commi 3 e 4 e art. 41 comma 1 lett. b,
         *     DL 331/93; prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici 
         *     ex art. 7-sexies lett. f, g, DPR 633/72 eart. 74-sexies, DPR 633/72) 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.TipoCassa', $Natura);
        /*
         * RiferimentoAmministrazione: formato alfanumerico; lunghezzamassima di 20 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.DatiCassaPrevidenziale.TipoCassa', $RiferimentoAmministrazione);
    }
    
    public function setDatiGeneraliDocumentoScontoMaggiorazione($Tipo, $Percentuale, $Importo)
    {        
        /*
         * Tipo: formato alfanumerico; lunghezza di 2 caratteri; i valori ammessi sono i seguenti:  
         * SC  sconto
         * MG  maggiorazione 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.ScontoMaggiorazione.Tipo', $Tipo);
        /* Percentuale: formato numerico nel quale i decimali vanno separati dall’intero con il carattere ‘.’ (punto). 
         * La sua lunghezza va da 4 a 6 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.ScontoMaggiorazione.Percentuale', $Percentuale);
        /* Importo: formato numerico nel quale i decimali vanno separati dall’intero con il carattere ‘.’ (punto).
         * La sua lunghezza va da 4 a 15 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.ScontoMaggiorazione.Importo', $Importo);
    }
    
    public function setDatiOrdineAcquisto($RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {
        $basePath = 'FatturaElettronicaBody.DatiGenerali.DatiOrdineAcquisto';
        $this->setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG);        
    }
    
    public function setDatiContratto($RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {
        $basePath = 'FatturaElettronicaBody.DatiGenerali.DatiContratto';
        $this->setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG);
    }
    
    public function setDatiConvenzione($RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {
        $basePath = 'FatturaElettronicaBody.DatiGenerali.DatiConvenzione';
        $this->setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG);
    }
    
    public function setDatiRicezione($RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {
        $basePath = 'FatturaElettronicaBody.DatiGenerali.DatiRicezione';
        $this->setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG);        
    }
    
    public function setDatiFattureCollegate($RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {
        $basePath = 'FatturaElettronicaBody.DatiFattureCollegate';
        $this->setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG);        
    }
    
    public function setDatiSal($RiferimentoFase)
    {
        // 2.2.9.3 Dati Riferimento Sal Blocco da valorizzare nei casi di fattura per stato di avanzamento  
        /* 
         * RiferimentoFase: formato numerico; lunghezza massima di 3 caratteri.
         */
        $this->dict->set('FatturaElettronicaBody.DatiSAL.RiferimentoFase', $RiferimentoFase);
    }
    
    public function setDatiDDT($NumeroDDT, $DataDDT, $RiferimentoNumeroLinea)
    {
        /*
         * NumeroDDT: formato alfanumerico; lunghezza massima di 20 caratteri.  
         */
        $this->dict->set('FatturaElettronicaBody.DatiDDT.NumeroDDT', $NumeroDDT);
        /*
         * DataDDT: la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiDDT.DataDDT', $DataDDT);
        /*
         * RiferimentoNumeroLinea: formato numerico; lunghezza massima di 4 caratteri. 
         */
        $this->dict->set('FatturaElettronicaBody.DatiDDT.RiferimentoNumeroLinea', $RiferimentoNumeroLinea);
    }
    
    /**
     * Metodo che setta i dati di trasporto della fattura
     * 
     * @param type $MezzoTrasporto formato alfanumerico; lunghezza massima di 80 caratteri.
     * @param type $CausaleTrasporto formato alfanumerico; lunghezza massima di 100 caratteri.
     * @param type $NumeroColli formato numerico; lunghezza massima di 4 caratteri. 
     * @param type $Descrizione formato alfanumerico; lunghezza massima di 100 caratteri. 
     * @param type $UnitaMisuraPeso formato alfanumerico; lunghezza massima di 10 caratteri.
     * @param type $PesoLordo formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 7 caratteri. 
     * @param type $PesoNetto formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 7 caratteri. 
     * @param type $DataOraRitiro la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MMDDTHH:MM:SS.
     * @param type $DataInizioTrasporto la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD.
     * @param type $TipoResa codifica del termine di resa (Incoterms) espresso secondo lo standard ICC-Camera di Commercio Internazionale (formato alfanumerico di 3 caratteri) 
     * @param type $DataOraConsegna la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MMDDTHH:MM:SS.
     */
    public function setDatiTrasporto($MezzoTrasporto, $CausaleTrasporto, $NumeroColli, $Descrizione, $UnitaMisuraPeso, $PesoLordo, $PesoNetto, $DataOraRitiro, $DataInizioTrasporto, $TipoResa, $DataOraConsegna)
    {        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.MezzoTrasporto', $MezzoTrasporto);     
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.CausaleTrasporto', $CausaleTrasporto);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.NumeroColli', $NumeroColli);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.Descrizione', $Descrizione);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.UnitaMisuraPeso', $UnitaMisuraPeso);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.PesoLordo', $PesoLordo);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.PesoNetto', $PesoNetto);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.DataOraRitiro', $DataOraRitiro);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.DataInizioTrasporto', $DataInizioTrasporto);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.TipoResa', $TipoResa);        
        $this->dict->set('FatturaElettronicaBody.DatiTrasporto.DataOraConsegna', $DataOraConsegna);
    }
    
    public function setDatiTrasportoDatiAnagraficiVettore($PartitaIva, $CodiceFiscale, $RagioneSociale = '', $Nome = '', $Cognome = '', $Titolo = '', $CodiceEORI = '', $IdPaese = 'IT')
    {        
        $basePath = 'FatturaElettronicaBody.DatiTrasporto.DatiAnagraficiVettore';
        $this->setModelAnagrafica($basePath, $IdPaese, $PartitaIva, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI);        
    }
    
    public function setDatiTrasportoIndirizzoResa($Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione)
    {
        $basePath = 'FatturaElettronicaBody.DatiTrasporto.IndirizzoResa';
        $this->setModelIndirizzo($basePath, $Indirizzo, $NumeroCivico, $Cap, $Comune, $Provincia, $Nazione);        
    }
    
    /**
     * Setta i dati della fattura principale
     * 
     * @param type $NumeroFatturaPrincipale formato alfanumerico; lunghezza massima di 20 caratteri. 
     * @param type $DataFatturaPrincipale la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD.
     */
    public function setFatturaPrincipale($NumeroFatturaPrincipale, $DataFatturaPrincipale)
    {        
        $this->dict->set('FatturaElettronicaBody.FatturaPrincipale.NumeroFatturaPrincipale', $NumeroFatturaPrincipale);        
        $this->dict->set('FatturaElettronicaBody.FatturaPrincipale.DataFatturaPrincipale', $DataFatturaPrincipale);
    }
    
    /**
     * Modelche aiuta a compilare le svariate anagrafiche presenti in file della fatturazione elettronica
     * 
     * @param string $basePath indirizzo base dei dati
     * @param string $IdPaese codice del paese assegnante l’identifcativo fiscale al soggetto trasmittente. 
     * @param string $IdCodice  numero di  identificazione fiscale del trasmittente (per i soggetti stabiliti nel territorio dello Stato Italiano corrisponde al Codice Fiscale; per i non residenti si fa riferimento all’identificativo fiscale assegnato dall’autorità del paese di residenza).
     *               In caso di IdPaese uguale a IT, il sistema ne verifica la presenza in Anagrafe Tributaria: se non esiste come codice fiscale, il file viene scartato con codice errore 00300. 
     * @param type $CodiceFiscale numero di codice fiscale del cedente/prestatore. Il sistema ne verifica la presenza in Anagrafe Tributaria: se non esiste come codice fiscale l file viene scartato con codice errore 00302.
     * @param type $RagioneSociale ditta, denominazione o ragione sociale del cedente/prestatore del bene/servizio da valorizzare nei casi di persona non fisica. 
     * @param type $Nome nome del cedente/prestatore del bene/servizio da valorizzare nei casi di persona fisica. 
     * @param type $Cognome cognome del cedente/prestatore del bene/servizio da valorizzare nei casi di persona fisica. 
     * @param type $Titolo  titolo onorifico del cedente/prestatore.
     * @param type $CodiceEORI numero del Codice EORI (Economic Operator Registration and Identification)  in base al Regolamento (CE) n. 312 del 16 aprile 2009. In vigore dal 1 Luglio 2009 tale codice identifica gli operatori economici nei rapporti con le autorità doganali sull'intero territorio dell'Unione Europea. 
     * @param type $IdLabel
     */
    private function setModelAnagrafica($basePath, $IdPaese, $IdCodice, $CodiceFiscale, $RagioneSociale, $Nome, $Cognome, $Titolo, $CodiceEORI, $IdLabel = 'IdFiscaleIVA')
    {        
        if ($IdCodice) {
            $this->setFieldValue($basePath,$IdLabel.'.IdPaese', $IdPaese);            
            $this->setFieldValue($basePath,$IdLabel.'.IdCodice', $IdCodice);
        }
        $this->setFieldValue($basePath,'CodiceFiscale', strtoupper($CodiceFiscale));
        $this->setFieldValue($basePath,'Anagrafica.Denominazione', $RagioneSociale);        
        $this->setFieldValue($basePath,'Anagrafica.Nome', $Nome);        
        $this->setFieldValue($basePath,'Anagrafica.Cognome', $Cognome);        
        $this->setFieldValue($basePath,'Anagrafica.Titolo', $Titolo);        
        $this->setFieldValue($basePath,'Anagrafica.CodiceEORI', $CodiceEORI);
    }
    
    /**
     * 
     * @param string  $basePath indirizzo base dei dati
     * @param integer $RiferimentoNumeroLinea formato numerico; lunghezza massima di 4 caratteri. 
     * @param string  $IdDocumento formato alfanumerico; lunghezza massima di 20 caratteri.
     * @param date    $Data la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD. 
     * @param string  $NumItem formato alfanumerico; lunghezza massima di 20 caratteri.
     * @param string  $CodiceCommessaConvenzione formato alfanumerico; lunghezza massima di 100 caratteri.
     * @param string  $CodiceCUP formato alfanumerico; lunghezza massima di 15 caratteri. 
     * @param string  $CodiceCIG formato alfanumerico; lunghezza massima di 15 caratteri. 
     */
    private function setModelEstremiDocumento($basePath, $RiferimentoNumeroLinea, $IdDocumento, $Data, $NumItem, $CodiceCommessaConvenzione, $CodiceCUP, $CodiceCIG)
    {        
        $fields = ['RiferimentoNumeroLinea', 'IdDocumento', 'Data', 'NumItem', 'CodiceCommessaConvenzione', 'CodiceCUP', 'CodiceCIG'];
        foreach($fields as $field) {            
            $this->setFieldValue($basePath, $field, $$field);            
        }        
    }
    
    private function setFieldValue($basePath, $field, $value, $force = false)
    {
        if (empty($value) and !$force) {
            return;           
        }
        if (substr($basePath, -1) != '.') {
            $basePath .= '.';
        }        
        $this->dict->set($basePath.$field, trim($value));
    }
    
    /**
     * Model per il settaggio degli indirizzi sede (gli elementi indicati di seguito fanno riferimento alla sede legale per le società e del domicilio fiscale per le ditte individuali e i lavoratori autonomi)
     * 
     * @param string $basePath Percordo base dei dati all'interno dell'XML
     * @param string $Indirizzo  indirizzo del cedente/prestatore del bene/servizio; deve essere valorizzato con il nome della via, piazza, etc., comprensivo, se si vuole, del numero civico. 
     * @param string $NumeroCivico numero civico relativo all’indirizzo specificato nell'elemento precedente; si può omettere se già riportato nell’elemento precedente.
     * @param string $Cap Codice di Avviamento Postale relativo all’indirizzo.
     * @param string $Comune Comune cui si riferisce l’indirizzo. 
     * @param string $Provincia sigla della provincia di appartenenza del comune (da valorizzare nei soli casi di sede in Italia).
     * @param string $Nazione codice della nazione.
     */ 
    private function setModelIndirizzo($basePath, $Indirizzo, $NumeroCivico, $CAP, $Comune, $Provincia, $Nazione = 'IT')
    {                                                      
        $fields = ['Indirizzo', 'NumeroCivico', 'CAP', 'Comune', 'Provincia', 'Nazione'];
        foreach($fields as $field) {
            $value = $$field;
            $this->setFieldValue($basePath, $field, $value);
        }        
    }
    
    /**
     * Aggiunge una riga di dettaglio al body della fattura
     * 
     * @param int     $NumeroLinea Numero progressivo della riga all'interno della fattura. Formato numerico; lunghezza massima di 4 caratteri.
     * @param string  $Descrizione formato alfanumerico; lunghezza massima di 1000 caratteri
     * @param decimal $Quantita formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 21 caratteri. 
     * @param decimal $PrezzoUnitario formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da  4 a 21 caratteri.
     * @param decimal $PrezzoTotale  formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 21 caratteri. 
     * @param decimal $AliquotaIVA formato numerico nel quale i decimali vanno separati dall'intero con il carattere  '.' (punto). La sua lunghezza va da 4 a 6 caratteri. 
     * @param string  $UnitaMisura formato alfanumerico; lunghezza massima di 10 caratteri.
     * @param string  $TipoCessionePrestazione formato alfanumerico. Lunghezza 2 caratteri. Valori ammessi sono: SC (Sconto), (PR) Premio, AB (Abbuono), AC (Spesa accessoria)
     * @param string  $CodiceTipo formato alfanumerico; lunghezza massima di 35 caratteri (Parte del campo CodiceArticolo)
     * @param string  $CodiceValore formato alfanumerico; lunghezza massima di 35 caratteri (Parte del campo CodiceArticolo)
     * @param string  $ScontoMaggiorazioneTipo Tipo di sconto, valori ammessi ()
     * @param decimal $ScontoMaggiorazionePercentuale
     * @param decimal $ScontoMaggiorazioneImporto
     * @param date    $DataInizioPeriodo  la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD. 
     * @param date    $DataFinePeriodo  la data deve essere rappresentata secondo il formato ISO 8601:2004, con la seguente precisione: YYYY-MM-DD. 
     * @param string  $Ritenuta Ritenuta: formato alfanumerico; lunghezza di 2 caratteri; il valore ammesso è: SI  linea di fattura soggetta a ritenuta
     * @param string  $Natura formato alfanumerico; lunghezza di 2 caratteri; i valori ammessi sono i seguenti:
     *                N1    escluse ex art.15
     *                N2    non soggette
     *                N3    non imponibili
     *                N4    esenti
     *                N5    regime del margine / IVA non esposta in fattura 
     *                N6    inversione contabile (per le operazioni in reverse charge ovvero nei casi di autofatturazione per acquisti extra UE di servizi ovvero per importazioni di beni nei soli casi previsti) 
     *                N7    IVA assolta in altro stato UE (vendite a distanza ex art. 40 commi 3 e 4 e art. 41 comma 1 lett. b, DL 331/93; prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, DPR 633/72 e art. 74-sexies, DPR 633/72) 
     * @param string $RiferimentoAmministrazione formato alfanumerico; lunghezza massima di 20 caratteri.
     * @return void
     */
    public function appendRiga($NumeroLinea, $Descrizione, $Quantita, $PrezzoUnitario, $PrezzoTotale, $AliquotaIVA, $UnitaMisura = null, $TipoCessionePrestazione = null, $CodiceTipo = null, $CodiceValore = null, $ScontoMaggiorazioneTipo = null, $ScontoMaggiorazionePercentuale = null, $ScontoMaggiorazioneImporto = null, $DataInizioPeriodo = null, $DataFinePeriodo = null, $Ritenuta = null, $Natura = null, $RiferimentoAmministrazione = null, $altro = null)
    {
        $row = [
            'NumeroLinea' => $NumeroLinea,
            'Descrizione' => $Descrizione,
            'Quantita' => number_format($Quantita, 2),
            // 'PrezzoUnitario' => number_format($PrezzoUnitario, 8, '.', ''),
            'PrezzoUnitario' => number_format($PrezzoTotale/$Quantita, 8, '.', ''),
            'PrezzoTotale' => number_format($PrezzoTotale, 2, '.', ''),
            'AliquotaIVA' => number_format($AliquotaIVA, 2, '.', '')
        ];
        if ($Natura) {
            $row['Natura'] = $Natura;
        }
        if ($UnitaMisura) {
            //$row['UnitaMisura'] = $UnitaMisura;
        }
        if ($TipoCessionePrestazione) {
            $row['TipoCessionePrestazione'] = $TipoCessionePrestazione;
        }
        if ($CodiceTipo && $CodiceValore) {
            $row['CodiceArticolo'] = [
                'CodiceTipo' => $CodiceTipo,
                'CodiceValore' => $CodiceValore
            ];
        }
        if ($DataInizioPeriodo && $DataFinePeriodo) {
            $row['DataInizioPeriodo'] = $DataInizioPeriodo;
            $row['DataFinePeriodo'] = $DataFinePeriodo;
        }
        if ($ScontoMaggiorazioneTipo && ($ScontoMaggiorazionePercentuale || $ScontoMaggiorazioneImporto)) {            
            $row['ScontoMaggiorazione'] = ['Tipo' => $ScontoMaggiorazioneTipo];
        }
        if ($ScontoMaggiorazioneTipo && $ScontoMaggiorazionePercentuale) {            
            $row['ScontoMaggiorazione']['Percentuale'] = $ScontoMaggiorazionePercentuale;
        }
        if ($ScontoMaggiorazioneTipo && $ScontoMaggiorazioneImporto) {            
            $row['ScontoMaggiorazione']['Importo'] = $ScontoMaggiorazioneImporto;
        }       
        if ($altro) {
            $row['AltriDatiGestionali'] = $altro;
        }
        $this->detailInvoice[] = ['DettaglioLinee' => $row];        
    }

    public function appendRigaRaw($row) {
        $this->detailInvoice[] = ['DettaglioLinee' => $row];        
    }

    public function setDatiRiepilogo($AliquotaIVA, $Natura, $SpeseAccessorie, $Arrotondamento, $ImponibileImporto, $Imposta, $EsigibilitaIVA, $RiferimentoNormativo)
    {        
        $summaryId = number_format($AliquotaIVA,2,'.','').$Natura;
        
        if (!array_key_exists($summaryId, $this->summaryInvoice)) {
            $dat = ['AliquotaIVA' => number_format($AliquotaIVA, 2, '.', '')];
            if (!empty($Natura)) {
               $dat['Natura'] = $Natura;
            }
            $dat['ImponibileImporto'] = 0.00;
            $dat['Imposta'] = 0.00;
            $dat['EsigibilitaIVA'] = $EsigibilitaIVA;
            if (!empty($RiferimentoNormativo)) {
                $dat['RiferimentoNormativo'] = $RiferimentoNormativo;
            }
            $this->summaryInvoice[$summaryId] = [
                'DatiRiepilogo' => $dat
            ];
        }                
        if (!empty($SpeseAccessorie)) {
            $this->summaryInvoice[$summaryId]['DatiRiepilogo']['SpeseAccessorie'] = number_format($SpeseAccessorie,2,'.','');
        }
        if (!empty($Arrotondamento)) {
            $this->summaryInvoice[$summaryId]['DatiRiepilogo']['Arrotondamento'] = number_format($Arrotondamento,2,'.','');
        }
        if (!empty($ImponibileImporto)) {
            $this->summaryInvoice[$summaryId]['DatiRiepilogo']['ImponibileImporto'] = number_format($ImponibileImporto,2,'.','');
        }
        if (!empty($Imposta)) {
            $this->summaryInvoice[$summaryId]['DatiRiepilogo']['Imposta'] =  number_format($Imposta,2,'.','');
        }  
        if (!empty($RiferimentoNormativo)) {
            $this->summaryInvoice[$summaryId]['DatiRiepilogo']['RiferimentoNormativo'] .= $RiferimentoNormativo;
        }
        $this->summaryInvoice[$summaryId]['DatiRiepilogo']['ImponibileImporto'] = number_format($this->summaryInvoice[$summaryId]['DatiRiepilogo']['ImponibileImporto'],2,'.','');
        $this->summaryInvoice[$summaryId]['DatiRiepilogo']['Imposta'] = number_format($this->summaryInvoice[$summaryId]['DatiRiepilogo']['Imposta'],2,'.','');
    }
    
    /**
     * 
     * @param type $condizionePagamento 
     *                TP01 : pagamento a rate
     *                TP02 : pagamento completo
     *                TP03 : anticipo
     * 
     * // i seguenti parametri o sono valori singoli o sono array della stessa lunghezza
     * 
     * @param type $modalitaList 
     *                MP01 contanti
     *                MP02 assegno
     *                MP03 assegno circolare
     *                MP04 contanti presso Tesoreria
     *                MP05 bonifico
     *                MP06 vaglia cambiario
     *                MP07 bollettino bancario
     *                MP08 carta di credito
     *                MP09 RID
     *                MP10 RID utenze
     *                MP11 RID veloce
     *                MP12 Riba
     *                MP13 MAV
     *                MP14 quietanza erario stato
     *                MP15 giroconto su conti di contabilità speciale
     *                MP16 domiciliazione bancaria
     *                MP17 domiciliazione postale
     * 
     * @param type $scadenzaList 
     * @param type $importoList 
     */
    public function setDatiPagamento($condizionePagamento, $modalitaList, $scadenzaList, $importoList, $check = null)
    {
        $idx = 0;
        $pag = ['type'=>$condizionePagamento, 'data'=>[]];
        $addPagamento = function($modalita, $scadenza, $importo) use (&$pag, $check) {
            if ($scadenza) {
                $p = [
                    'ModalitaPagamento'=> $modalita,
                    'DataScadenzaPagamento'=> $scadenza?:' ', 
                    'ImportoPagamento'=> number_format($importo, 2, '.', '')
                ];
            } else {
                $p= [
                    'ModalitaPagamento'=> $modalita,
                    'ImportoPagamento'=> number_format($importo, 2, '.', '')
                ];
            }
            if ($check) {
                $p = call_user_func($check, $p);
            }
            $pag['data'] []= $p;
        };
        
        if (!is_array($modalitaList)) {
            $addPagamento($modalitaList, $scadenzaList, $importoList);
        } else {
            // Assert::true(count($modalitaList) == count($scadenzaList) == count($importoList))
            for($i=0; $i<count($modalitaList); $i++) {
                $addPagamento($modalitaList[$i], $scadenzaList[$i], $importoList[$i]);
            }
        }
        $this->modalitaPagamento[] = $pag;
    }
    
    public function loadFakeData()
    {
        $this->setDatiTrasmissione('02336370693','INV00001','info@pec.spinit.it','0000000','+393494421929','pietro.celeste@gmail.com'); 
        $this->setCedentePrestatore('RF01','02336370693','02336370693','SPIN IT S.R.L.','Pietro','Celeste','Dott.','56789');
        $this->setCedentePrestatoreSede('Via Fontuccia','25','66016','Guardiagrele','CH','Italia');
        $this->setCedentePrestatoreStabileOrganizzazione('Via Fontuccia','25','66016','Guardiagrele','CH','Italia');
        $this->setCedentePrestatoreIscrizioneRea('CH', '0220202', '10000.00');
        $this->setCedentePrestatoreContatti('Pietro Celeste','087185943','087185943','p.celeste@spinit.it');
        $this->setCedentePrestatoreRappresentanteFiscale('', 'ERMSTL25E04', '', 'Ermanno', 'Astolfi','Dott.');
        $this->setCedentePrestatoreRappresentateFiscaleSede('Via Fontuccia','25','66016','Cappelle sul Tavo','PE','Italia');
        $this->setTerzoIntermediarioOSoggettoEmittente('02336370693', '02336370693', 'SPIN IT S.R.L.', 'Ermanno', 'Astolfi','Dott.');
        $this->appendRiga(1,'T460S i5 6300 8gb ram 512 ssd','1','400','400','22.00','PZ',null,'EAN','BOXNUC8I5BEH2');
        $this->appendRiga(2,'Nuc 8gen. i5 8209','1','350.00','350.00','22.00','PZ',null,'EAN','T460S');
        $this->appendRiga(3,'Articolo di prova','1','10.00','10.00','22.00','PZ',null,'EAN','PROVA');
    }
    
    public function get($key, $default = null)
    {
        return $this->dict->get($key, $default);
    }
}
